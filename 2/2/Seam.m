function [O]= Seam(I,SCALE,HIT)
tStart=tic;

GRD=Strength(I);
[M N]=size(GRD);
%imshow(GRD);

%{
%finding min candiidates
MINSORT=sort(DP(M,:,1));

for i=drange(1,HIT)
    for i1=drange(1,N)
        if(MINSORT(i)==DP(M,i1,1) && DELMARK(M,i1)==0)
            DELMARK(M,i1)=1;
            break;
        end
    end
end

%Propogating them backward
    for j=drange(1,N)
        if(DELMARK(M,j)==1)
            COL=DP(M,j,2);
            for i=drange(1,M-1)
                DELMARK(M-i,COL)=1;
                COL=DP(M-i,COL,2);
            end
        end
    end
imshow(DELMARK);
%}     


if strcmp(SCALE,'DOWN')
%    MIN=min(DP(M,:,1));
%    MINCOL=find(DP(M,:,1)==MIN);

%    DELMARK(M,MINCOL)=1;
    %Propogating them backward

%    COL=DP(M,MINCOL,2);
%    for i=drange(1,M-1)
%        DELMARK(M-i,COL)=1;
%        COL=DP(M-i,COL,2);
%    end    
    
    OUTIMG=zeros(M,N-HIT,3);
    DELMARK=getSeam(GRD,HIT);
    for i=drange(1,M)
        j1=1;
        for j=drange(1,N)
            if(DELMARK(i,j)==0)
                OUTIMG(i,j1,1)=I(i,j,1);
                OUTIMG(i,j1,2)=I(i,j,2);
                OUTIMG(i,j1,3)=I(i,j,3);            
                j1=j1+1;
            end
        end
    end
    O=uint8(OUTIMG);
elseif strcmp(SCALE,'UP')
    DELMARK=getSeam(GRD,HIT);    
    OUTIMG=zeros(M,N+HIT,3);
    for i=drange(1,M)
        j1=1;
        for j=drange(1,N)
                OUTIMG(i,j1,1)=I(i,j,1);
                OUTIMG(i,j1,2)=I(i,j,2);
                OUTIMG(i,j1,3)=I(i,j,3);            
                j1=j1+1;                
            if(DELMARK(i,j)==1)
                OUTIMG(i,j1,1)=I(i,j,1);
                OUTIMG(i,j1,2)=I(i,j,2);
                OUTIMG(i,j1,3)=I(i,j,3);                            
                j1=j1+1;
            end
        end
    end    
    O=uint8(OUTIMG);
else
    
end

toc(tStart)
end