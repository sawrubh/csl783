function[NO]=MultiSeam(I,HITX,HITY)
    NO=uint8(I);

    if(HITX<0)
        NO=(Seam(NO,'DOWN',abs(HITX)));
    else
        NO=(Seam(NO,'UP',abs(HITX)));        
    end
    
    NO=imrotate(NO,90);

    if(HITY<0)
    NO=(Seam(NO,'DOWN',abs(HITY)));
    else
    NO=(Seam(NO,'UP',abs(HITY)));        
    end    
    
    NO=imrotate(NO,-90);

    size(I)
    size(NO)
end