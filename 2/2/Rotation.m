function[THETA]=Rotation(I1,I2)
P1=CartToPolar(I1);
P2=CartToPolar(I2);
[THETA R]=Translation(P1,P2);
end