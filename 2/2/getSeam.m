function[O] = getSeam(GRD,NUM)


[M N]=size(GRD);

DP=zeros(M,N,2);
MAXG=max(max(GRD));

%Initializing DP
    for j=drange(1,N)
        DP(1,j,1)=GRD(1,j);
    end
    
DELMARK=zeros(M,N);

for k=drange(1,NUM)
    for i = drange(2,M)
        for j=drange(1,N)
                        
            if (j>=2)  
                if ((DELMARK(i-1,j-1)==0))
                LEFT=DP(i-1,j-1,1);
                else
                LEFT=2*M*MAXG;                    
                end
            else
                LEFT=2*M*MAXG;
            end

            if(j+1<=N ) 
                if(DELMARK(i-1,j+1)==0)
                RIGHT=DP(i-1,j+1,1);
                else
                RIGHT=2*M*MAXG;    
                end
            else
                RIGHT=2*M*MAXG;
            end

            if (DELMARK(i-1,j)==0)
                CENTER=DP(i-1,j,1);
            else
                CENTER=2*M*MAXG;
            end

            MIN=min(min(LEFT,RIGHT),CENTER);
            DP(i,j,1)=MIN+GRD(i,j);

            if(MIN==CENTER)
                DP(i,j,2)=j;
            elseif(MIN==LEFT)
                DP(i,j,2)=j-1;
            elseif(MIN==RIGHT)
                DP(i,j,2)=j+1; 
            end

        end
    end
        
        MIN=2*M*MAXG;
        MINCOL=0;
        for i=drange(1,N)
            if(DELMARK(M,i)==0)
                if(MIN>DP(M,i,1))
                    MINCOL=i;
                    MIN=DP(M,i,1);
                end 
            end
        end        
        DELMARK(M,MINCOL)=1;
        %Propogating them backward

        COL=DP(M,MINCOL,2);
        for i=drange(1,M-1)
            DELMARK(M-i,COL)=1;
            COL=DP(M-i,COL,2);
        end
end
imshow(DELMARK);

O=DELMARK;
end