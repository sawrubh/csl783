function[OFFX OFFY]= Translation(I1,I2)
[M N C1]=size(I1);
if(C1>1)
G1=rgb2gray(I1);
G2=rgb2gray(I2);
else
    G1=I1;
    G2=I2;
end

F1=fft2(G1);
F2=fft2(G2);

FS1=fftshift(F1);
FS2=fftshift(F2);

PRODUCT=FS1.*conj(FS2);
PHASE=(PRODUCT./abs(PRODUCT));

IPH=ifft2(PHASE);
imshow(IPH);
MAX=max(max(IPH));
[R C]=find(IPH==MAX);

[N+1-C M+1-R];

OFFX=N+1-C;
OFFY=M+1-R;
end