function[O] = imTranslate(I,OFFX,OFFY)
[M N C]=size(I);
OUT=zeros(M,N,3);

X=mod(1+OFFY,M+1);
X=X+(X==0);
for i=drange(1,M)
   Y=mod(1+OFFX,N+1);
   Y=Y+(Y==0);
   for j=drange(1,N)
       OUT(X,Y,1)=I(i,j,1);
       OUT(X,Y,2)=I(i,j,2);
       OUT(X,Y,3)=I(i,j,3);       

       Y=mod(Y+1,N+1);
       Y=Y+(Y==0);
   end
   X=mod(X+1,M+1);
   X=X+(X==0);
end

O=uint8(OUT);
imshow(O);
end