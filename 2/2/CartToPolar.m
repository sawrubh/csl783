function [O]=CartToPolar(I)
[M N C]=size(I);
RAD=floor(sqrt((M/2)^2+(N/2)^2));
%OUT=zeros(ceil(RAD),360,3);

for i=drange(1,RAD)
    for j=drange(1,360)
        [X Y]=(pol2cart((j-1)*2*pi/360,i-1));
        X=X+M/2;
        Y=Y+N/2;
        if(X>=0 && X<=M && Y<=N && Y>=0)
        X=round(X);
        Y=round(Y);
        X=X+(X==0);
        Y=Y+(Y==0);
        
        if C>1
        OUT(i,j,:)=I(X,Y,:);
        else
        OUT(i,j)=I(X,Y);            
        end
        
        end
    end
end

imshow(OUT);
O=uint8(OUT);
end