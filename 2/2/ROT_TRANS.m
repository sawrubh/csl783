function[]=ROT_TRANS(I1,I2)
G1=rgb2gray(I1);
G2=rgb2gray(I2);

F1=fft2(G1);
F2=fft2(G2);

FS1=fftshift(F1);
FS2=fftshift(F2);

M1=abs(FS1);
M2=abs(FS2);

[Theta]=Rotation(M1,M2);
l
end