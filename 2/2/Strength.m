function[O]=Strength(I)

G=[0 -1 0;-1 4 -1;0 -1 0];
BW=rgb2gray(I);
BW=double(BW);
GRD=conv2(BW,G,'same');
GRD=(GRD<0).*(-GRD)+((GRD>=0).*GRD);
GRD=GRD/(max(max(GRD)));
O=GRD;
end