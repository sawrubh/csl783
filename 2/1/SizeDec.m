function y = SizeDec(img,x)

I = imread(img);
X = rgb2gray(I);
Sobel =zeros(3,3);

Sobel(1,1) = 0;
Sobel(1,2) = -1;
Sobel(1,3) = 0;
Sobel(2,1) = -1;
Sobel(2,2) = 4;
Sobel(2,3) = -1;
Sobel(3,3) = 0;
Sobel(3,1) = 0;
Sobel(3,2) = -1;

Energy = conv2(X,Sobel,'same');
Energy = abs(Energy);
%%imshow(Energy/max(max(Energy)));
y=I;

for i=drange(1,x/2)
y = Seam(y,Energy);
end

for i=drange(1,x/2)
   y=SeamH(y,Energy); 
end

imshow(y);

end