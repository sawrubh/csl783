function shift = findTranslate(image1, image2)
fftOf1 = fft2(double(image1));
fftOf2 = fft2(double(image2));
num = fftOf1 .* conj(fftOf2);
den = abs(fftOf1 .* fftOf2);
phase = num ./ den;
invPhase = ifft2(phase);
maxVal = max(max(invPhase));
[x, y] = find(maxVal == invPhase);
if x < size(image1, 2)/2
    x = 1 - x;
else
    x = size(image1, 2) - x;
end
if y < size(image1, 1)/2
    y = 1 - y;
else
    y = size(image1, 1) - y;
end
shift = [x, y];
end