function y=SeamH(In,E,k)

Energy = E;
I = In;
[a b C]=size(I);

M=zeros(a,b);
Path=zeros(a,b);
NewImg = zeros(a+k,b,C);
MySeam = zeros(a,b);

for m=drange(1,k)

for j = drange(1,a)
    M(j,1) = Energy(j,1);
    Path(1,j) = 0;
end

for i = drange(2,b)
   
    for j = drange(1,a)
       
        if(j==1)
            if(M(j,i-1) > M(j+1,i-1) )
                Path(j,i) =1;
                M(j,i) = Energy(j,i)+M(j+1,i-1);
            else    
                Path(j,i) =0;
                M(j,i) = Energy(j,i)+M(j,i-1);
            end 
        elseif(j==a)
            if(M(j,i-1) > M(j-1,i-1) )
                Path(j,i) =-1;
                M(j,i) = Energy(j,i)+M(j-1,i-1);
            else    
                Path(j,i) =0;
                M(j,i) = Energy(j,i)+M(j,i-1);
            end 
            
        else
           if(M(j,i-1) > M(j-1,i-1))
               if(M(j-1,i-1)>M(j+1,i-1))
                  Path(j,i) = 1;
                  M(j,i) = Energy(j,i) + M(j+1,i-1);
               else
                   Path(j,i) = -1;
                  M(j,i) = Energy(j,i) + M(j-1,i-1);
               end
               
           else 
               
               if(M(j,i-1)>M(j+1,i-1))
                   Path(j,i) = 1;
                  M(j,i) = Energy(j,i) + M(j+1,i-1);
               else
                   Path(j,i) = 0;
                  M(j,i) = Energy(j,i) + M(j,i-1);
               end
               
           end
        end
               
    end
    
end

min = M(1,b);
num =1;

for i = drange(2,a) 

   if(min>M(i,b))
      min = M(i,b);
      num =i;
   end
    
end

for i = drange(1,b)

    MySeam(num,b-i+1) = 1;
    Energy(num,b-i+1) =Energy(num,b-i+1)+1000;
    num = num + Path(num,b-i+1);
    
end

end
%%imshow(MySeam);

 for i=drange(1,b)
            count=0;
        for j = drange(1,a)
           
            if(MySeam(j,i)==0)
               
                NewImg(j+count,i,:) = I(j,i,:);
                
            else
                
                NewImg(j+count,i,:) = I(j,i,:);
                 NewImg(j+1+count,i,:) = I(j,i,:);
                 count=count+1;
                
            end  
        end
 end   

NewImg = uint8(NewImg);
y=NewImg;


end