function toReturn = findTranslateAndRotate(image1, image2)
fftOf1 = abs(fft2(double(image1)));
fftOf2 = abs(fft2(double(image2)));
theta = findRotate(fftOf1, fftOf2);
rotated11 = imrotate(image1, theta);
[x1, y1] = findTranslate(rotated11, image2);
rotated12 = imrotate(image1, -theta);
[x2, y2] = findTranslate(rotated12, image2);
if imtranslate(rotated11, shift1) == image2
    toReturn = [theta, x1, y1];
else
    toReturn = [-theta, x2, y2];
end
end