function y=Seam(I,Energy)

[a b C]=size(I);

M=zeros(a,b);
Path=zeros(a,b);

for j = drange(1,b)
    M(1,j) = Energy(1,j);
    Path(1,j) = 0;
end

for i = drange(2,a)
   
    for j = drange(1,b)
       
        if(j==1)
            if(M(i-1,j) > M(i-1,j+1) )
                Path(i,j) =1;
                M(i,j) = Energy(i,j)+M(i-1,j+1);
            else    
                Path(i,j) =0;
                M(i,j) = Energy(i,j)+M(i-1,j);
            end 
        elseif(j==b)
            if(M(i-1,j) > M(i-1,j-1) )
                Path(i,j) =-1;
                M(i,j) = Energy(i,j)+M(i-1,j-1);
            else    
                Path(i,j) =0;
                M(i,j) = Energy(i,j)+M(i-1,j);
            end 
            
        else
           if(M(i-1,j) > M(i-1,j-1))
               if(M(i-1,j-1)>M(i-1,j+1))
                  Path(i,j) = 1;
                  M(i,j) = Energy(i,j) + M(i-1,j+1);
               else
                   Path(i,j) = -1;
                  M(i,j) = Energy(i,j) + M(i-1,j-1);
               end
               
           else 
               
               if(M(i-1,j)>M(i-1,j+1))
                   Path(i,j) = 1;
                  M(i,j) = Energy(i,j) + M(i-1,j+1);
               else
                   Path(i,j) = 0;
                  M(i,j) = Energy(i,j) + M(i-1,j);
               end
               
           end
        end
               
    end
    
end

min = M(a,1);
num =1;

for i = drange(2,b) 

   if(min>M(a,i))
      min = M(a,i);
      num =i;
   end
    
end

NewImg = zeros(a,b-1,C);
MySeam = zeros(a,b);

for i = drange(1,a)
   
    if(num==1)
       
        NewImg(a-i+1,1:b-1,:) = I(a-i+1,2:b,:);
        
    elseif(num==b)
         NewImg(a-i+1,1:b-1,:) = I(a-i+1,1:b-1,:);
    else
         NewImg(a-i+1,1:num-1,:) = I(a-i+1,1:num-1,:);
         NewImg(a-i+1,num:b-1,:) = I(a-i+1,num+1:b,:);
    end
    
    MySeam(a-i+1,num) = 1;
    num = num + Path(a-i+1,num);
    
end

NewImg = uint8(NewImg);
y=NewImg;

end