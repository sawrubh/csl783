function rotation = findRotate(image1, image2)
rot1 = imgpolarcoord(image1);
rot2 = imgpolarcoord(image2);
fftOf1 = fft2(double(rot1));
fftOf2 = fft2(double(rot2));
num = fftOf1 .* conj(fftOf2);
den = abs(fftOf1 .* fftOf2);
phase = num ./ den;
invPhase = ifft2(phase);
maxVal = max(max(invPhase));
[~, y] = find(maxVal == invPhase);
if y < size(image1, 1)/2
    y = 1 - y;
else
    y = size(image1, 1) - y;
end
rotation = y;
end