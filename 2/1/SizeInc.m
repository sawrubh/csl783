function y = SizeInc(img,k)

I = imread(img);

X = rgb2gray(I);
Sobel =zeros(3,3);

Sobel(1,1) = 0;
Sobel(1,2) = -1;
Sobel(1,3) = 0;
Sobel(2,1) = -1;
Sobel(2,2) = 4;
Sobel(2,3) = -1;
Sobel(3,3) = 0;
Sobel(3,1) = 0;
Sobel(3,2) = -1;

Energy = conv2(X,Sobel,'same');
Energy = abs(Energy);

y=SeamIV(I,Energy,k/2);
X2=rgb2gray(y);
Energy = conv2(X2,Sobel,'same');
Energy = abs(Energy);
y=SeamIH(y,Energy,k/2);

imshow(y);

end
