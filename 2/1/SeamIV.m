function y=SeamIV(In,E,k)

Energy = E;
I = In;
[a b C]=size(I);

M=zeros(a,b);
Path=zeros(a,b);
MySeam = zeros(a,b);
NewImg = zeros(a,b+k,C);

for m = drange(1,k)
    
for j = drange(1,b)
    M(1,j) = Energy(1,j);
    Path(1,j) = 0;
end

for i = drange(2,a)
   
    for j = drange(1,b)
       
        if(j==1)
            if(M(i-1,j) > M(i-1,j+1) )
                Path(i,j) =1;
                M(i,j) = Energy(i,j)+M(i-1,j+1);
            else    
                Path(i,j) =0;
                M(i,j) = Energy(i,j)+M(i-1,j);
            end 
        elseif(j==b)
            if(M(i-1,j) > M(i-1,j-1) )
                Path(i,j) =-1;
                M(i,j) = Energy(i,j)+M(i-1,j-1);
            else    
                Path(i,j) =0;
                M(i,j) = Energy(i,j)+M(i-1,j);
            end 
            
        else
           if(M(i-1,j) > M(i-1,j-1))
               if(M(i-1,j-1)>M(i-1,j+1))
                  Path(i,j) = 1;
                  M(i,j) = Energy(i,j) + M(i-1,j+1);
               else
                   Path(i,j) = -1;
                  M(i,j) = Energy(i,j) + M(i-1,j-1);
               end
               
           else 
               
               if(M(i-1,j)>M(i-1,j+1))
                   Path(i,j) = 1;
                  M(i,j) = Energy(i,j) + M(i-1,j+1);
               else
                   Path(i,j) = 0;
                  M(i,j) = Energy(i,j) + M(i-1,j);
               end
               
           end
        end
               
    end
    
end

min = M(a,1);
num =1;

for i = drange(2,b) 

   if(min>M(a,i))
      min = M(a,i);
      num =i;
   end
    
end

for i = drange(1,a)

    MySeam(a-i+1,num) = 1;
    Energy(a-i+1,num) =Energy(a-i+1,num) + 1000;
    num = num + Path(a-i+1,num);
    
end

end

%%imshow(MySeam);

   
    for i=drange(1,a)
            count=0;
        for j = drange(1,b)
           
            if(MySeam(i,j)==0)
               
                NewImg(i,j+count,:) = I(i,j,:);
                
            else
                
                NewImg(i,j+count,:) = I(i,j,:);
                 NewImg(i,j+1+count,:) = I(i,j,:);
                 count=count+1;
                
            end  
        end
    end   


NewImg = uint8(NewImg);
y=NewImg;

end