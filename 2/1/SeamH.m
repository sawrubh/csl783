function y=SeamH(I,Energy)

[a b C]=size(I);

M=zeros(a,b);
Path=zeros(a,b);

for j = drange(1,a)
    M(j,1) = Energy(j,1);
end

for i = drange(2,b)
   
    for j = drange(1,a)
       
        if(j==1)
            if(M(j,i-1) > M(j+1,i-1) )
                Path(j,i) =1;
                M(j,i) = Energy(j,i)+M(j+1,i-1);
            else    
                Path(j,i) =0;
                M(j,i) = Energy(j,i)+M(j,i-1);
            end 
        elseif(j==a)
            if(M(j,i-1) > M(j-1,i-1) )
                Path(j,i) =-1;
                M(j,i) = Energy(j,i)+M(j-1,i-1);
            else    
                Path(j,i) =0;
                M(j,i) = Energy(j,i)+M(j,i-1);
            end 
            
        else
           if(M(j,i-1) > M(j-1,i-1))
               if(M(j-1,i-1)>M(j+1,i-1))
                  Path(j,i) = 1;
                  M(j,i) = Energy(j,i) + M(j+1,i-1);
               else
                   Path(j,i) = -1;
                  M(j,i) = Energy(j,i) + M(j-1,i-1);
               end
               
           else 
               
               if(M(j,i-1)>M(j+1,i-1))
                   Path(j,i) = 1;
                  M(j,i) = Energy(j,i) + M(j+1,i-1);
               else
                   Path(j,i) = 0;
                  M(j,i) = Energy(j,i) + M(j,i-1);
               end
               
           end
        end
               
    end
    
end

min = M(1,b);
num =1;

for i = drange(2,a) 

   if(min<M(i,b))
      min = M(i,b);
      num =i;
   end
    
end

NewImg = zeros(a-1,b,C);
MySeam = zeros(a,b);

for i = drange(1,b)
   
    if(num==1)
       
        NewImg(1:a-1,b+1-i,:) = I(2:a,b+1-i,:);
        
    elseif(num==a)
         NewImg(1:a-1,b+1-i,:) = I(1:a-1,b+1-i,:);
    else
         NewImg(1:num-1,b+1-i,:) = I(1:num-1,b+1-i,:);
         NewImg(num:a-1,b+1-i,:) = I(num+1:a,b+1-i,:);
    end
    MySeam(num,b-i+1) = 1;
    num = num + Path(num,b-i+1);
    
end
NewImg = uint8(NewImg);
y=NewImg;


end