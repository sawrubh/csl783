function [result] = bin(i, j)
    result = min(max(i, j), 6);
end