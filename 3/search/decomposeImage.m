function [result] = decomposeImage(T)
    [x, y] = size(T);
    for i = 1:1:x
        output(i, :) = decomposeArray(T(i, :));
    end
    output = output';
    for i = 1:1:y
        output(i, :) = decomposeArray(output(i, :));
    end
    result = output';
end