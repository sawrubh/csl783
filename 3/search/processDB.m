function [goodBank, badBank, t] = processDB(dbPath, m)
    files = dir(dbPath);
    numFiles = length(files);
    imgPath = strcat(dbPath,'/');
    % Using 3 because 1 and 2 are . and ..
    imgPath = strcat(imgPath, files(3).name);
    input = imresize(imread(imgPath), [512, 512]);
    [x, y, color] = size(input);

    goodArray{color, x, y} = [];
    badArray{color, x, y} = [];

    for i = 3:1:numFiles
        imgPath = strcat(dbPath,'/');
        imgPath = strcat(imgPath, files(i).name);
        input = rgb2ntsc(imresize(imread(imgPath), [512, 512]));
        transformedImage = input;
        for j = 1:1:color
            decomposedImage = decomposeImage(transformedImage(:, :, j));
            T(j, i) = decomposedImage(1, 1);
            truncatedMap = truncateArray(decomposedImage, m);
            for k = 1:1:length(truncatedMap)
                if (truncatedMap(k).coefficient > 0)
                    goodArray{j, truncatedMap(k).X, truncatedMap(k).Y} = [goodArray{j, truncatedMap(k).X, truncatedMap(k).Y}, i];
                else
                    badArray{j, truncatedMap(k).X, truncatedMap(k).Y} = [badArray{j, truncatedMap(k).X, truncatedMap(k).Y}, i];
                end
            end
        end
    end

    goodBank = goodArray;
    badBank = badArray;
    t = T;
end