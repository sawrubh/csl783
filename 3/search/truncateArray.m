function [result] = truncateArray(I, m)
    marker = 1;
    [x, y] = size(I);
    
    for i = 1:x
        for j = 1:y
            MAP(marker).coefficient = I(i,j);
            MAP(marker).magnitude = abs(I(i, j));
            MAP(marker).X = i;
            MAP(marker).Y = j;            
            marker = marker + 1;
        end
    end
    
    MAP(1).magnitude = -1;
    [~, sortIndex] = sort([MAP(:).magnitude], 'descend');
    result = [];
    for i = 1:1:m
        result = [result, MAP(sortIndex(i))];
    end
end
