load db.mat;
Q = imread('..\query\4.jpg');
scores = scoreQuery(imresize(Q, [512, 512]), goodBank, badBank, T, m, W);
[~,minIndex] = min(scores(3 : end));
files = dir(dbPath);
imshow(imread(strcat(strcat(dbPath, '\'),files(minIndex+2).name)));