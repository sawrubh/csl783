function [output] = scoreQuery(Q, goodBank, badBank, T, m, weights)
    [~, ~, color] = size(Q);
    [~, numFiles] = size(T);
    scores = zeros(numFiles, 1);
    Q = rgb2ntsc(Q);
    for i = 1:1:color
        wavelet = decomposeImage(Q(:,:,i));
        for j = 3:1:numFiles
            scores(j) = scores(j) + weights(i, 1)*abs(wavelet(1, 1) - T(i, j));
        end
        wavelet_n = truncateArray(Q, m);
        for j = 1:1:length(wavelet_n)
            if wavelet_n(j).coefficient > 0
                list = goodBank{i, wavelet_n(j).X, wavelet_n(j).Y};
            else
                list = badBank{i, wavelet_n(j).X, wavelet_n(j).Y};                
            end
            for l = 1:1:length(list)
                scores(list(l)) = scores(list(l)) - weights(i, bin(wavelet_n(j).X, wavelet_n(j).Y));
            end
        end
    end
    output = scores;
end