function [decomposed] = decomposeArray(A)
    h = length(A);
    A = A/sqrt(h);
    while h > 1
        h = h/2;
        for i = 0:1:h-1
            Ai(i+1) = (A(2*i+1) + A(2*i+2))/sqrt(2);
            Ai(h+i+1) = (A(2*i+1) - A(2*i+2))/sqrt(2);
        end
        A = Ai;
    end
    decomposed = A;
end