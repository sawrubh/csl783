function [output] = quantImage(pmr, qlevel)
    for i = 1:length(pmr)
        temp = zeros(size(pmr{i}));
        for j = 1:3
            xx = pmr{i};
            t1 = xx(:, :, j);
            threshold = multithresh(xx(:, :, j), qlevel);
            v1 = [min(t1(:)) threshold];
            v2 = [threshold max(t1(:))];
            temp(:, :, j) = double(imquantize(xx(:, :, j), threshold, (v1 + v2)/2));
        end
        pmr{i} = temp;
    end
    output = pmr;