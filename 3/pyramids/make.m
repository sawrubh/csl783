i1 = imread('apple1.jpg');
i2 = imread('orange1.jpg');
i1 = im2double(i1);
i2 = im2double(i2);
i1 = imresize(i1, [size(i2, 1) size(i2, 2)]);

v = 230;
level = 5;
lpa = generatePyramid(i1, 'lap', level);
lpb = generatePyramid(i2, 'lap', level);

maska = zeros(size(i1));
maska(:, 1:v, :) = 1;
maskb = 1 - maska;
blurh = fspecial('gauss', 30, 15);
maska = imfilter(maska, blurh, 'replicate');
maskb = imfilter(maskb, blurh, 'replicate');

limgo = cell(1,level);
for p = 1:level
	[Mp, Np, ~] = size(lpa{p});
	maskap = imresize(maska,[Mp Np]);
	maskbp = imresize(maskb,[Mp Np]);
	limgo{p} = lpa{p}.*maskap + lpb{p}.*maskbp;
end
imgo = reconPyramid(limgo);
figure,imshow(imgo)
imgo1 = maska.*i1+maskb.*i2;
figure,imshow(imgo1)