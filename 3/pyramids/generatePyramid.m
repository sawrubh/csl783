function [pyramid] = generatePyramid(img, type, level)
    pyramid = cell(1, level);
    pyramid{1} = im2double(img);
    for i = 2:level
        pyramid{i} = reducePyramid(pyramid{i - 1});
    end
    if strcmp(type,'gauss')
        return;
    end

    for i = level-1:-1:1
    	osz = size(pyramid{i+1})*2-1;
    	pyramid{i} = pyramid{i}(1:osz(1),1:osz(2),:);
    end

    for i = 1:level-1
    	pyramid{i} = pyramid{i}- expandPyramid(pyramid{i+1});
    end
end