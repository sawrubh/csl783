function [output] = expandPyramid(img)
kernelWidth = 5;
centreWidth = .375;
ker1d = [.25-centreWidth/2 .25 centreWidth .25 .25-centreWidth/2];
kernel = kron(ker1d, ker1d') * 4;

ker00 = kernel(1:2:kernelWidth, 1:2:kernelWidth);
ker01 = kernel(1:2:kernelWidth, 2:2:kernelWidth);
ker10 = kernel(2:2:kernelWidth, 1:2:kernelWidth);
ker11 = kernel(2:2:kernelWidth, 2:2:kernelWidth);
img = im2double(img);
l = size(img(:, :, 1));
osz = l*2-1;
output = zeros(osz(1),osz(2),size(img,3));

for i = 1:size(img, 3)
    img1 = img(:,:,i);
    img1ph = padarray(img1,[0 1],'replicate','both');
    img1pv = padarray(img1,[1 0],'replicate','both');
    
    img00 = imfilter(img1,ker00,'replicate','same');
    img01 = conv2(img1pv,ker01,'valid');
    img10 = conv2(img1ph,ker10,'valid');
    img11 = conv2(img1,ker11,'valid');
    
    output(1:2:osz(1),1:2:osz(2),i) = img00;
    output(2:2:osz(1),1:2:osz(2),i) = img10;
    output(1:2:osz(1),2:2:osz(2),i) = img01;
    output(2:2:osz(1),2:2:osz(2),i) = img11;
end
end