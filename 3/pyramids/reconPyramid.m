function [output] = reconPyramid(pyramid)
    for p = length(pyramid)-1:-1:1
        pyramid{p} = pyramid{p}+ expandPyramid(pyramid{p+1});
    end
    output = pyramid{1};
end

