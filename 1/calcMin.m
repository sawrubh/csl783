function y = calcMin(pixel, bins)
min = norm(double(bins(1,:)) - double(pixel), 2);
y = bins(1, :);
for i = 1:size(bins, 1)
    d = norm(double(bins(i, :)) - double(pixel), 2);
    if d < min
        y = bins(i, :) ;
        min = d;
    end
end
end