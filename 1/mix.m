function mix(img, numOfBins)
edge = dog(img);
qM = quantMedian(img, numOfBins);
[edgeX, edgeY, ~] = size(edge);
for i = 1:edgeX
    for j = 1:edgeY
        for k = 1:3
            qM(i, j, k) = qM(i, j, k) * edge(i, j, 1);
        end
    end
end
imshow(qM);
end