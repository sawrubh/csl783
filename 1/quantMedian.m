function y = quantMedian(img, numOfBins)
input = imread(img);
tic
[indexedInput, map] = rgb2ind(input, 65536);
cutPosition = myMedian(map, numOfBins);
for i = 1:size(map, 1)
    map(i, :) = calcMin(map(i, :), cutPosition);
end
toc
imshow(indexedInput, map);
y = ind2rgb(indexedInput, map);
end