function y = myMedian(matrix, bins)
if bins == 1
    y = mean(matrix, 1);
else
    lengthAxis = [max(matrix(:,1))-min(matrix(:,1)),max(matrix(:,2))-min(matrix(:,2)),max(matrix(:,3))-min(matrix(:,3))];
    [~, maxIndex] = max(lengthAxis);
    matrix = sortrows(matrix, maxIndex);
    if rem(bins, 2) == 0
        bins1 = bins/2;
        bins2 = bins/2;
    else
        bins1 = floor(bins/2);
        bins2 = (bins+1)/2;
    end
    if rem(length(matrix), 2) == 0
        idx = floor(length(matrix)/2);
        y = [myMedian(matrix(1:idx, :), bins1); myMedian(matrix(idx+1:end, :), bins2)];
    else
        idx = floor(length(matrix)/2)+1;
        y = [myMedian(matrix(1:idx-1, :), bins1); myMedian(matrix(idx:end, :), bins2)];
    end
end
end