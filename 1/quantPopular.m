function y = quantPopular(img, numOfBins)
inputRGB = imread(img);
tic
[inputX, inputY, ~] = size(inputRGB);
colorBox = zeros(256, 256, 256);
bins = zeros(numOfBins, 3);
for i = 1:inputX
    for j = 1:inputY
        r = inputRGB(i, j, 1);
        g = inputRGB(i, j, 2);
        b = inputRGB(i, j, 3);
        colorBox(r+1, g+1, b+1) = colorBox(r+1, g+1, b+1) + 1;
    end
end
for i = 1:numOfBins
    [~, ind1] = max(colorBox);
    [~, ind2] = max(max(colorBox));
    [~, ind3] = max(max(max(colorBox)));
    maxColor(3) = ind3;
    maxColor(2) = ind2(ind3);
    maxColor(1) = ind1(1,ind2(ind3),ind3);
    bins(i,:) = maxColor;
    colorBox(maxColor(1), maxColor(2), maxColor(3)) = 0;
end
for i = 1:inputX
    for j = 1:inputY
        temp = [inputRGB(i, j, 1) inputRGB(i, j, 2) inputRGB(i, j, 3)];
        inputRGB(i, j, :) = calcMin(temp, bins);
    end
end
toc
imshow(inputRGB);
y = inputRGB;
end