function y = dog(img)
I=imread(img);
X = rgb2gray(I);
sig2 = 2;
sig1 = 1;
k=5;
Z1 = zeros(2*k+1);
Z2 = zeros(2*k+1);
root = sqrt(2*pi);
coeff1 = 1/(sig1*root);
coeff2 = 1/(sig2*root);
coeff1 = (coeff1)^2;
coeff2 = (coeff2)^2;
for i = drange(1,2*k+1)
    r1 = (i-k)*(i-k) ;
    for j = drange(1,2*k+1)
        r2 = (j-k)*(j-k);
        r = r1+r2;
        Z1(i,j) = coeff1.*exp((-1).*r/(2*sig1*sig1));
        Z2(i,j) = coeff2.*exp((-1).*r/(2*sig2*sig2));
    end
end
E1 = conv2(X,Z1,'same');
E2 = conv2(X,Z2,'same');
tau = 1;
sigh = 4;
F = (E1) - tau*E2;
epsilon = 0;
F=(F > epsilon).*1+(F<=epsilon).*(1+tanh(F.*sigh));
imshow(F);
y = F;
end


