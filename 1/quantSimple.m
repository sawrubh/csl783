function y = quantSimple(img, numOfColors)
input = imread(img);
tic
[inputX, inputY, ~] = size(input);
if numOfColors < 0
    disp('The number of colors in the final image should be a positive number');
else
    ratio = 255/(numOfColors^(1/3));
end
for i = 1:inputX
    for j = 1:inputY
        input(i, j, 1) = ((floor(input(i, j, 1)/ratio) + ceil(input(i, j, 1)/ratio))/2)*ratio;
        input(i, j, 2) = ((floor(input(i, j, 2)/ratio) + ceil(input(i, j, 2)/ratio))/2)*ratio;
        input(i, j, 3) = ((floor(input(i, j, 3)/ratio) + ceil(input(i, j, 3)/ratio))/2)*ratio;
    end
end
toc
imshow(input);
y = input;
end